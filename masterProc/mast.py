#!/usr/bin/python3
import subprocess

ccloc = "/home/andrew/catercloud/"


def run_command_in_dir(directory: str, command: str):
    process = subprocess.Popen(command, cwd=directory, shell=True)
    return process


p1 = run_command_in_dir(ccloc+'server/', './server')
p2 = run_command_in_dir(ccloc+'freqCounter/target/debug/', './freqCounter')
p3 = run_command_in_dir(ccloc+'stringCleaner', './cleaner')
p4 = run_command_in_dir(ccloc+'subjectFinder', './sf.py')
p5 = run_command_in_dir(ccloc+'wordMap', './plotter.py')

# Wait for all processes to complete
p1.wait()
p2.wait()
p3.wait()
p4.wait()
p5.wait()
