
function getNouns() {
    let targpath = "/nouns";

    $.ajax({
        async: false, //until 1.0
        type: "POST",
        url: targpath,
        dataType: "json",
        contentType: 'application/json; charset=utf-8',
        success: function (dat) {
            localStorage.setItem("nouns", JSON.stringify(dat));
        },
        error: function (a, b, c) {
            console.log("ABC:", a, b, c);
        }
    });

    return;
}


//Pull in and return the word frequency data as json, turned into an object
function getFreqs() {
    let targpath = "/frequencyJson";

    $.ajax({
        async: false, //For testing purposes
        type: "POST",
        url: targpath,
        dataType: "json",
        contentType: 'application/json; charset=utf-8',
        success: function (dat) {
            localStorage.setitem("freq-dat", JSON.stringify(dat));
            getFreqRange(dat);
        },
        error: function (a, b, c) {
            console.log("Sendoff FAIL:", b, c)
        },
    });
    return;
}


//Iterate over the freqJson and set high/low number count values
function getFreqRange(jsonDataObj) { //Input: {"Word": "whole", "Count": 1}

    let low = 0.0
    let hig = 0.
    //First iteration to find biggest
    for (const iter of jsonDataObj.Data) {
        if (iter.Count > hig) {
            hig = iter.Count;
        }
    }

    //Now, to count back down to find smallest
    low = hig;

    //Start at the newly found biggest, then look for the lowest down from that 
    for (const currIter of jsonDataObj.Data) {
        if (currIter.Count < low) {
            low = currIter.Count;
        }
    }

    console.log("Set high/low:", hig, low);
    localStorage.setItem("freq-low", low);
    localStorage.setItem("freq-high", hig);

    return

}



/*
    Originally, nouncloud and wordcloud data were handled through two completely independent 
    stacks. This is an attempt to buid a generic cloud builder to cut down on LOC and 
    to allow for future possible expansion.
*/
function genericCloudBuilder(mode) {

    //The cloud that we're building into.
    targElem = null;
    wordData = "";//Plaintext word data as json
    wordDict = {};//Dictionary containing 

    if (mode == "noun") {//Build nouncloud
        console.log("CLOUD BUILD MODE: noun");
        getNouns();
        wordData = localStorage.getItem("nouns")

        targElem = document.getElementById("nouncloud");
    } else if (mode == "words") {
        console.log("CLOUD BUILD MODE: words");
        getFrequs();
        wordData = localStorage.getItem("freq-data")

        targElem = document.getElementById("cloud");
    } else {
        //TODO: Error stuff
    }
    wordData = wordData.replace(/Noun/gi, "Word")
    console.log("Word data:", wordData);
    dat = JSON.parse(wordData);
    targElem.innerHTML = "";
    prev = "";

    //Iterate over the word data and build the word cloud
    for (let elem of dat.Data) {

        console.log("COUNT:", elem["Count"]);

        customLabel = "data-size-class=\"" + elem["Count"] + "\"";
        classLabel = "class=\"wordspan _" + elem["Count"] + " nounspan\"";
        idLabel = "id=\"" + elem["Word"] + "\"";
        hyperLink = "";
        oClick = "onclick=\"popModal(this); genLines('" + elem["Word"] + "')\"";
        divTotal = "<span " + customLabel + " " + oClick + classLabel + idLabel + ">" + hyperLink + elem["Word"] + "</span>";

        prev = targElem.innerHTML;
        targElem.innerHTML = targElem.innerHTML + divTotal;
    }

    //Set the sizes of the span elements in the cloud============================================

    //Get the elements in the cloud
    words = document.getElementsByClassName("wordspan");
    console.log("trWords:", words);
    wordDict = {};
    itrCount = 0;

    for(var elem of words){
        itrCount += 1;
        targGrab = document.getElementById(elem.id);
        tg = targGrab.getAttribute('data-size-class');
        console.log("DS:", tg, itrCount);
        wordDict[elem.id] = parseFloat(tg);
    }

    //Resize the elements of the dictionary to match the sizes we want to set
    rear = setMapRange(wordDict, 50, 300);
    //Iterate over the word data and set the sizes of the span elements in the cloud
    for (let elem in rear) {
        targelem = document.getElementById(elem);
        targelem.style.fontSize = rear[elem] + "%";
        if(mode == "words") {
            console.log("ELEM THING:", targelem.style.fontSize);
        }
    }

    return;

}


function _setMapRange(dictionary, minRange, maxRange) {
    const values = Object.values(dictionary);
    const sortedValues = values.sort((a, b) => a - b);
    const range = maxRange - minRange;
    const step = range / (sortedValues.length - 1);
  
    const rearrangedDictionary = {};
  
    for (let i = 0; i < sortedValues.length; i++) {
      const newValue = minRange + step * i;
      const originalKey = Object.keys(dictionary).find(
        (key) => dictionary[key] === sortedValues[i]
      );
  
      rearrangedDictionary[originalKey] = newValue;
    }
  
    return rearrangedDictionary;
  }
  
  //Automagic resize range values to be in range of lower/upper bounds
  function setMapRange(dict, lower, upper) {
    let min = Math.min(...Object.values(dict));
    let max = Math.max(...Object.values(dict));
    for (let key in dict) {
        dict[key] = lower + (dict[key] - min) * (upper - lower) / (max - min);
    }
    return dict;
}