



  
  function handleResize(){
    buildPlot();
  }
   
  




// Function to map a value from one range to another
function mapRange(value, inMin, inMax, outMin, outMax) {
  return ((value - inMin) * (outMax - outMin)) / (inMax - inMin) + outMin;
}

function buildPlot(){

  //console.log("MR Test:", mapRange(14, 22,15,88,3));

    //JQ ajaz call to get our source for semantic relationship mapping.
    var jsonData={};
    $.ajax({
      type: "POST",
      url:"/mappoints",
      async: false,
      success: function(dat){
        console.log("Points:", dat);
        jsonData = dat;
      },
      error: function(a,b,c){
        console.log("ABC:", a,b,c);
      }
    });
  
    // Get the canvas element
    var canvas = document.getElementById("plotCanvas");
    const ctx = canvas.getContext("2d");

    // Get the container element so we can set the canvas to the right size
    const masterElem = canvas.parentElement;
    canvas.width = masterElem.offsetWidth * .95;
    
    console.log("CANTEST ORIGIN:", canvas.width);

    // Define the canvas dimensions
    const canvasWidth = canvas.width;
    const canvasHeight = canvas.height;

    // Map the range of x and y values to the canvas dimensions
    const xMin = -15;  // Minimum x value in the data
    const xMax = 15;   // Maximum x value in the data
    const yMin = -15;  // Minimum y value in the data
    const yMax = 15;   // Maximum y value in the data

    // Add a click event listener to the canvas
    canvas.addEventListener('click', function(event) {
      const rect = canvas.getBoundingClientRect(); // Get the canvas size and position
      const mouseX = event.clientX - rect.left;   // Calculate the relative X position of the click
      const mouseY = event.clientY - rect.top;    // Calculate the relative Y position of the click

      // Iterate through the data points
      data.forEach((wordData) => {
        const x = mapRange(wordData.carts.xval, xMin, xMax, 0, canvas.width);
        const y = mapRange(wordData.carts.yval, yMin, yMax, 0, canvas.height);

        // Check if the click coordinates are within the range of the point
        if (Math.abs(mouseX - x) <= 5 && Math.abs(mouseY - y) <= 5) {
          alert(wordData.keystr);

          addToSubs(wordData.keystr);
          
          return;
        }
      });
    });

    // Plot each word in the JSON data
    const data = jsonData.DATA;
    data.forEach((wordData) => {
      console.log("CANTEST wid", canvasWidth);
      console.log("CANTEST hei", canvasHeight);

      const x = mapRange(wordData.carts.xval, xMin, xMax, 0, canvasWidth);
      const y = mapRange(wordData.carts.yval, yMin, yMax, 0, canvasHeight);

      console.log("PLOTTING: ", x,y)
      
      // Draw a circle representing the word at its corresponding (x, y) position
      ctx.beginPath();
      ctx.arc(x, y, 5, 0, 2 * Math.PI);
      ctx.fillStyle = "rgba(200,20,20,0.5)";
      ctx.fill();
      ctx.closePath();
      
      // Display the word next to the circle
      ctx.fillStyle = "black";
      ctx.font = "12px Arial";
      ctx.fillText(wordData.keystr, x + 10, y);
    });
}

function addToSubs(word){
  let ssInner = sessionStorage.getItem("clusterSelect");
  let fullstr = ssInner+"<span id=\""+word+"\" class=\"clusterselect\">"+word+"</span>"
  
  document.getElementById("clusterList").innerHTML = fullstr;
  sessionStorage.setItem("clusterSelect",fullstr);

}

function clearSubs(){
  sessionStorage.setItem("clusterSelect","");
}