#!/usr/bin/python3
import mysql.connector
import spacy
import spacy.cli
import time
import sys

plaintextCache = {}
subjects = {}
nlp = spacy.load("en_core_web_trf")

def write_database_noun_data(to_send):
    print("\n\n\n\nSTARTING WRITE DATABASE\n==========================\n\n\n\n")
    dbcon = mysql.connector.connect(
        #host="bestlappy",
        host="localhost",
        user="catercloud",
        password="313231a",
        database="capperCloud"
    )

    curs = dbcon.cursor()
    print("\nsubjects:",to_send,"\n\n")
    for itr, cnt in to_send.items():
        print(f"\n\nITRCNT:{itr} || {cnt}\n\n")

        try:
            curs.execute(f"REPLACE INTO nouns (noun, count) VALUES ('{itr}','{cnt}');")
            dbcon.commit()
        except Exception as e:
            print(e)
    dbcon.close()

def load_database_data():
    #while True:
    dbcon = mysql.connector.connect(
        #host="bestlappy",
        host="localhost",
        user="catercloud",
        password="313231a",
        database="capperCloud"
    )
    curs = dbcon.cursor()
    curs.execute("SELECT text,scale FROM plaintext")
    res = curs.fetchall()
    for i in res:
        tmpVar = i[0].lower()        
        plaintextCache[tmpVar] = i[1]
    dbcon.close() 
        

def pull_subjects():
    print("Starting pull subjects")
    print("Sentences:", plaintextCache)
    tmpSub = {}
    sentences = []
    for line in plaintextCache:
        spl = line.split(".")
        for sp in spl:
            sentences.append(sp)
        for sntc in sentences:
            #print("Sentence:: -> ",sntc)
            doc = nlp(sntc)
            subjs = [token.text for sent in doc.sents for token in sent if token.dep_ == 'nsubj']# and token.pos_ != 'PRON']
            for sub in subjs:
                if sub in tmpSub:
                    tmpSub[sub] = tmpSub[sub]+1
                else: 
                    tmpSub[sub] = 1
    print(f"\n\nSetting tmpSubs:{tmpSub}\n\n")
    write_database_noun_data(tmpSub)


def main():

    #keep the transformer model up to date
    #spacy.cli.download("en_core_web_trf")    

    while True:
        load_database_data()
        print("\n\nLoaded database\n\n")
        pull_subjects()
        print("\n\n\n\nSubjects pulled")
        print("Subjects:", subjects)

        print("\n\nSet data in DB:\n\n")
        #time.sleep(1)
        
if __name__ == "__main__":
    main()
