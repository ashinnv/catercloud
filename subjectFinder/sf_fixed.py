#!/usr/bin/python3
import mysql.connector
import spacy
#import spacy.cli
import time

plaintextCache = {}
subjects = {}

def load_database_data():
    while True:
        dbcon = mysql.connector.connect(
            host="bestlappy",
            user="root",
            password="313231a",
            database="capperCloud"
        )
        curs = dbcon.cursor()
        curs.execute("SELECT text,scale FROM plaintext")
        res = curs.fetchall()
        for i in res:
            plaintextCache[i[0]] = i[1]
        dbcon.close()
        time.sleep(1)

def pull_subjects():
    sentences = []
    for line in plaintextCache:
        spl = line.split(".")
        for sp in spl:
            sentences.push(sp)
        for sntc in sentences:
            doc = nlp(sntc)
            subjs = [token.text for sent in doc.sents for token in sent if token.dep_ == 'nsubj' and token.pos_ != 'PRON']
            print(subjs)