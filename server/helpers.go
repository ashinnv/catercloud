package main

import (
	"fmt"
	"os"
)

func read(targ string) []byte {
	fDat, ferr := os.ReadFile(targ)
	if ferr != nil {
		fmt.Println("Error reading file:", ferr)
	}

	return fDat

}

func getFinstring() string {
	return string(read("../htm/root.html"))
}

func getCss() string {
	return string(read("../htm/css.css"))
}

func getJs() string {
	return string(read("../htm/js.js"))
}

func getJq() string {
	return string(read("../htm/jq.js"))
}
