module gitlab.com/ashinnv/server

go 1.19

require (
	github.com/go-sql-driver/mysql v1.7.0
	golang.org/x/crypto v0.9.0
)
