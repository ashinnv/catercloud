package main

import (
	"fmt"
)

type ptxt struct {
	Id        int
	Timestamp uint64
	Username  string
	Text      string
	Scale     int
}

type ptxCont struct {
	Data []ptxt
}

type freq struct {
	Word  string
	Count uint
}

type freqCont struct {
	Data []freq
}

type userInput struct {
	Timestamp   uint64
	Uname       string
	Passwd      string
	Testimonial string
	Scale       uint64
}

func (hashMe *userInput) HashPass() error {
	passString := hashMe.Passwd
	hash, hashErr := hashPassword(passString)
	if hashErr != nil {
		fmt.Println("Dumping hash attempt due to error:", hashErr)
		return hashErr
	}

	hashMe.Passwd = hash
	fmt.Println("Hashed ", passString, "into", hash)

	return nil

}
