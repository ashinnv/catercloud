package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"golang.org/x/crypto/bcrypt"
)

type Noun struct {
	Noun  string
	Count uint
}

type nounCont struct {
	Data []Noun
}

type Cart struct {
	Xval float64
	Yval float64
}

type MapPoint struct {
	KeyStr string
	Carts  Cart
}

// For sending to the JS frontend
type MapDat struct {
	DATA []MapPoint
}

func genRoot() func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html; charset=UTF-8")

		finString := getFinstring()
		w.Write([]byte(finString))
	}
}

func genCss() func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/css")
		w.Write([]byte(getCss()))
	}
}

func genJs() func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/javascript")

		w.Write([]byte(getJs()))
	}
}

func genJq() func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/javascript")
		w.Write([]byte(getJq()))
	}
}

// Create a JSON object with a list of plaintext lines and serve those lines
func genPlaintext() func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {

		w.Header().Set("Content-Type", "application/json")

		fmt.Println("Starting genPlaintext")
		db, err := sql.Open("mysql", "catercloud:313231a@tcp(bestlappy:3306)/capperCloud")
		if err != nil {
			panic(err.Error())
		}
		defer db.Close()

		fmt.Print("Startingi genPlaintext sql req")

		//Run the request
		rows, err := db.Query("SELECT id, timestamp, username, text, scale FROM plaintext") // ORDER BY id DESC LIMIT 1")
		if err != nil {
			panic(err.Error())
		}
		defer rows.Close()

		//Append the rows to the return data
		var ptCont ptxCont
		var results []ptxt
		for rows.Next() {
			var p ptxt
			err := rows.Scan(&p.Id, &p.Timestamp, &p.Username, &p.Text, &p.Scale)
			if err != nil {
				panic(err.Error())
			}
			results = append(results, p)
			fmt.Print("Results len: ", len(results))
		}

		if err := rows.Err(); err != nil {
			panic(err.Error())
		} else {
			fmt.Println("Res:", results)
		}

		ptCont.Data = results
		fmt.Println("ptCont:", ptCont)

		marsh, marshErr := json.Marshal(&ptCont)
		if marshErr != nil {
			fmt.Println("MarshErr:", marshErr)
		}

		w.Write(marsh)
	}
}

func genImgRes() func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "image/png")

		// Get the filepath from the request URL
		val := strings.Split(r.URL.Path, "/")
		if len(val) < 1 {
			fmt.Println("Short split: ", val)
		}
		// Get the last part of the filepath
		filename := val[len(val)-1]
		fType := filepath.Ext(filename)

		//fmt.Print("\n\n\n=========================\nFilename:", filename, "\n\n")

		if fType == ".jpg" {
			w.Header().Set("Content-Type", "image/jpeg")
		} else if fType == ".png" {
			w.Header().Set("Content-Type", "image/png")
			//panic("Couldn't get proper file type from url path: " + filename)
		}

		fdat, gfbErr := getFilebytes(filename)
		if gfbErr != nil {
			panic("Error for reading file data: " + gfbErr.Error())
		}

		w.Write(fdat)

	}
}

func getFilebytes(fname string) ([]byte, error) {
	fDat, fErr := os.ReadFile("../img/" + fname)
	return fDat, fErr
}

func genListen() func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		var uip userInput
		dec := json.NewDecoder(r.Body)
		decErr := dec.Decode(&uip)
		if decErr != nil {
			fmt.Println("DecErr:", decErr)
			time.Sleep(5 * time.Second)
		} else {
			//fmt.Println("UIP: ", uip)
		}

		tmpStr := uip.Passwd

		if uip.HashPass() != nil {
			fmt.Println("DUMPING UIP HASHf")
		} else {
			fmt.Println("Checked:", uip.Passwd, "and", tmpStr, "and got", doPasswordsMatch(uip.Passwd, tmpStr))
		}

		//fmt.Println("THE MARRRRRRKKKKKKKKK")
		writeToSql(uip)
	}
}

// Grep through database data until we find the right thing. THIS COULD BE DONE SO MUCH BETTER
// Filter out punctuation during the search
func doAGrep(input string) /*[]string*/ []byte {

	var retArr []string
	//var _retArr []ptxt

	db, err := sql.Open("mysql", "catercloud:313231a@tcp(bestlappy:3306)/capperCloud")
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	fmt.Printf("\n===================\n\nRunning doAGrep sql req. Searching for: %s\n=============================\n", input)
	genQuery := fmt.Sprintf("SELECT id, timestamp, username, text, scale FROM plaintext WHERE text LIKE \"%% %s %%\"", input) // where text includes %s", input)

	//Run the request
	rows, err := db.Query(genQuery) // ORDER BY id DESC LIMIT 1")
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()

	//Append the rows to the return data
	var ptCont ptxCont
	var results []ptxt
	for rows.Next() {
		var p ptxt
		err := rows.Scan(&p.Id, &p.Timestamp, &p.Username, &p.Text, &p.Scale)
		if err != nil {
			panic(err.Error())
		}

		p.Text = " " + p.Text
		results = append(results, p)
		fmt.Println("Results len: ", len(results))
	}

	if err := rows.Err(); err != nil {
		panic(err.Error())
	} else {
		fmt.Println("Res:", results)
	}

	ptCont.Data = results

	/*
		for _, line := range ptCont.Data {
			a := strings.ToLower((line.Text))
			b := strings.ToLower(input)
			fmt.Printf("\nA: ||%s|| || B: ||%s||\n\n", a, b)

			if strings.Contains(a, " "+b+" ") {
				//sendLine := fmt.Sprintf("%s says: %s", line.Username, line.Text)
				retArr = append(retArr, sendLine)
			}
		}
	*/
	fmt.Printf("\n\nLines:\n%v\n\n", retArr)

	lineComp, jsnErr := json.Marshal(&ptCont)
	if jsnErr != nil {
		fmt.Println("MARSHAL ERROR:", jsnErr)
	}

	//return retArr
	return lineComp
}

// Return the lines of plaintext that contain the last item on the URL's path
func genLines() func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {

		targ := strings.TrimPrefix(r.URL.Path, "/grep/")
		fmt.Println("Targ is", targ)
		marshaledJson := doAGrep(targ)

		w.Write(marshaledJson)

	}
}

// Combined functionality of both genFreq() and serveNouns(). Needs more work to be truly generic.
func genericWordServer(mode string) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {

		w.Header().Set("Content-Type", "application/json")

		db, err := sql.Open("mysql", "catercloud:313231a@tcp(bestlappy:3306)/capperCloud")
		if err != nil {
			panic(err.Error())
		}
		defer db.Close()

		//Run the request
		if mode == "noun" {
			rows, err := db.Query("SELECT noun,count FROM nouns") // ORDER BY id DESC LIMIT 1")
			if err != nil {
				panic(err.Error())
			}
			defer rows.Close()

			//Append the rows to the return data
			var results []Noun
			for rows.Next() {
				var p Noun
				err := rows.Scan(&p.Noun, &p.Count)
				if err != nil {
					panic(err.Error())
				}
				results = append(results, p)
			}

			if err := rows.Err(); err != nil {
				panic(err.Error())
			}

			dcon := nounCont{Data: results}

			marsh, marshErr := json.Marshal(&dcon)
			if marshErr != nil {
				fmt.Println("MarshErr:", marshErr)
			}

			w.Write(marsh)
			fmt.Print("\n\n======\nIn noun mode generic handler\n\n")

		} else if mode == "word" {
			rows, err := db.Query("SELECT word, count FROM frequencyJson") // ORDER BY id DESC LIMIT 1")
			if err != nil {
				panic(err.Error())
			}
			defer rows.Close()

			var results []freq
			for rows.Next() {
				var p freq
				err := rows.Scan(&p.Word, &p.Count)
				if err != nil {
					panic(err.Error())
				}
				results = append(results, p)
			}
			if err := rows.Err(); err != nil {
				panic(err.Error())
			}

			fCo := freqCont{Data: results}
			marsh, marshErr := json.Marshal(&fCo)
			if marshErr != nil {
				fmt.Println("MarshError:", marshErr)
			}

			w.Write(marsh)
			fmt.Print("\n\n======\nIn freq mode generic handler\n\n")

		}

	}
}

/*
Pull in the data from the word plot as json
so that we can build a map on the JS side.
*/
func ServeMapJson() func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {

		db, err := sql.Open("mysql", "catercloud:313231a@tcp(bestlappy:3306)/capperCloud")
		if err != nil {
			panic(err.Error())
		}
		defer db.Close()

		//Run the request
		rows, err := db.Query("SELECT data FROM plot") // ORDER BY id DESC LIMIT 1")
		if err != nil {
			panic(err.Error())
		}
		defer rows.Close()
		//Append the rows to the return data
		//var res MapDat
		var res string
		for rows.Next() {
			err := rows.Scan(&res)
			if err != nil {
				panic(err.Error())
			}
		}
		/*
			marsh, marshErr := json.Marshal(&res)
			if marshErr != nil {
				fmt.Println("MarshErr:", marshErr)
			}
		*/
		w.Header().Set("Content-Type", "application/json")
		//w.Write(marsh)
		w.Write([]byte(res))
	}
}

// Serve up the JS that plots json dat xy values
func GenPlt() func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/javascript")
		pltDat, plErr := os.ReadFile("../htm/plotter.js")
		if plErr != nil {
			fmt.Println("ERROR IN SERVING GENPLT")
		}
		w.Write([]byte(pltDat))
	}

}

func hashPassword(password string) (string, error) {
	var passwordBytes = []byte(password)
	hashedPasswordBytes, err := bcrypt.GenerateFromPassword(passwordBytes, bcrypt.MinCost)
	return string(hashedPasswordBytes), err
}

func doPasswordsMatch(hashedPassword, currPassword string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(currPassword))
	return err == nil
}

func genBuilder() func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/javascript")

		dat, fileErr := ioutil.ReadFile("../htm/cloudBuilder.js")
		if fileErr != nil {
			fmt.Println("ERROR IN SERVING GENBUILDER:", fileErr)
		}
		w.Write(dat)
	}
}
