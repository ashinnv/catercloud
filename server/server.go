package main

import (
	"flag"
	"fmt"
	"net/http"
	"time"
)

func main() {

	certloc := flag.String("certloc", "", "certificate location")
	keyloc := flag.String("keyloc", "", "key location")
	flag.Parse()

	go runSvr(*certloc, *keyloc)

	for {
		time.Sleep(10000 * time.Hour)
	}
}

func runSvr(certloc string, keyloc string) {
	http.HandleFunc("/", genRoot())
	http.HandleFunc("/css", genCss())
	http.HandleFunc("/js", genJs())
	http.HandleFunc("/jq", genJq())
	http.HandleFunc("/plaintext", genPlaintext())
	//http.HandleFunc("/frequencyJson", genFreq())//===============================
	http.HandleFunc("/frequencyJson", genericWordServer("word"))//===============================
	http.HandleFunc("/imgres/", genImgRes())
	http.HandleFunc("/lstn", genListen())
	http.HandleFunc("/grep/", genLines())
	//http.HandleFunc("/nouns", serveNouns())//====================
	http.HandleFunc("/nouns", genericWordServer("noun"))//====================
	http.HandleFunc("/mappoints", ServeMapJson())
	http.HandleFunc("/plotter", GenPlt())
	http.HandleFunc("/builder", genBuilder())

	var launchErr error

	if certloc == "" {
		fmt.Println("Listening on 8080")
		launchErr = http.ListenAndServe(":8080", nil)
		if launchErr != nil {
			panic("Launch Error: " + launchErr.Error())
		}

	} else {
		fmt.Println("listening on 443")
		launchErr = http.ListenAndServeTLS(":443", certloc, keyloc, nil)
		if launchErr != nil {
			panic("Launch Error: " + launchErr.Error())
		}

	}

}
