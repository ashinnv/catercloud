# Catercloud
Catercloud is a project intended to ingest testimonials from a community and boil down the sentiments provided in a way that is easily digestible and quickly available. 

## Modes:
Currently, there are three modes, Wordcloud, Nouncloud, and Clustercloud mode. 
1. Wordcloud is exactly what it sounds like; a wordcloud of the words submitted by users, sized by their frequency. Filters are available to filter out more or less common words to keep the cloud clean and terse.Clicking the words should provide a modal with the word and its usages in the data
2. Nouncloud is a second implementation of the wordcloud, except that the words are filtered down to nouns server-side
3. Clustercloud (incomplete) goes deeper into processing the data by clustering together words on a cartesian grid by definition. This requires deeper dinvestionation of word data and (as of July 2023) is taking most of the effort. Current implementation is explained in the specific clustercloud session.

## Clustercloud: 
Clustering is done serverside using freely available data from sources like Project Gutenberg and Wikipedia across 5 steps. 

1. Create a list of individual words
2. Iterate over the words and find them in the training data.
3. Count distance to other words on a per-sentence basis.
4. Find the mean distance between each word and every other word that we have in the data. 
5. Use these distances to cluster the user-submitted words on a cartesian grid in order to create clusters of related entries in a way that allows the viewer to see how the users' entries group and relate to each other. 


## CONTRIBUTING: 
~ Snip
### TODO:

- SubjectFinder is missing subjects bad. Even with the transformer model, it is really missing the massive majority of subjects in the data. This is probably an issue with implementation and not an issue with spaCy specifically, but it still needs to be handled.

- There is a lack of consistency across the project. In some places, there is a function that does something monolithically; all in one function and yet, right down from there, there can be a function that does something almost identical and makes calls to several helper functions. There is also a lot of repeated effort that should be rolled up into one function. A good example is "serveNouns()" and "genFreq()" in the server's "handlers.go". These can both be the same function, but they are different and function differently. -- UPDATE: The process of doing this has begun. Much more work ahead.

- CSS consistency. Make the front-facing appearance consistent. 

- <s>Data-overload. There are three separate sections of data output. This should be pushed down to a single display at once. Already have a top bar for selecting these things.</s> 

- Json object translation: We have the nouns and general words being served as json. From one source, each json object is {"Word":"myword"} and on the other, it's {"Noun":"myword"}. This is being translated in the generic cloud builder to both be {"Word":"myword"}. This is caused because the two tables on the DB are labeled that way. To rename the columns would mean re-writing several programs in the stack to use the new syntax. Eventually, this should be done, but for now, it's being handled in JS. Ugly? Yes. Hacky? HELL YES. Still being done? Of course.