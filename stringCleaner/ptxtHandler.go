package main

type ptxt struct {
	Id        int
	Timestamp uint64
	Username  string
	Text      string
	Scale     int
}

type ptxCont struct {
	Data []ptxt
}

type freq struct {
	Word  string
	Count uint
}

type freqCont struct {
	Data []freq
}

type userInput struct {
	Timestamp   uint64
	Uname       string
	Passwd      string
	Testimonial string
	Scale       uint64
}

func (p *ptxCont) Add(input ptxt) {

	tmpStore := p.Data

	p.Data = append(tmpStore, input)
}
